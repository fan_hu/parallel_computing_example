import numpy as np
import time

start = time.time()
matrixA = np.random.rand(10000, 10000)
matrixB = np.random.rand(10000, 10000)
matrixC = np.dot(matrixA, matrixB)
end = time.time()
time_used = end - start
print(f"Time used: {time_used:.2f}")