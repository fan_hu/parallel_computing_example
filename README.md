## 提交python作业

### 创建Python虚拟环境

由于我们没有权限更改服务器上的python环境，因此我们需要在自己的家目录下创建一个属于自己的python环境。这可以通过`conda`来实现，具体操作如下。

1. 加载`conda`模块

``` shell
$ module load miniconda3/4.7.12.1-gcc-4.8.5
```

2. 利用`conda`创建虚拟环境；这一步中的 `myenv`只是虚拟环境的名称，大家可以根据喜好自己做修改。

``` shell
$ conda create -n myenv
```

3. 加载虚拟环境

``` shell
$ source activate myenv
```

4. 安装需要的包，这里我列入了基础的`numpy` ,`scipy`, `matplotlib` 和`pandas`，大家后续还可以通过`conda install`命令来安装其他自己想要的包，但要先activate虚拟环境或者使用`conda install -n myenv`来针对自己的虚拟环境进行操作

``` shell
$ conda install numpy scipy matplotlib pandas
```

5. 如果要退出虚拟环境，可以输入

``` shell
$ souce deactivate myenv
```

参考资料：

[conda文档](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#activating-an-environment)

[网上找的简易版资料](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/20/conda/)

### 提交作业

接下来在服务器提交job作业时，也要确保job中也启用了我们自己创建的python环境，因此我们可以创建表示环境的文件`env.sh`，并在其中写入以下内容：

```
module load miniconda3/4.7.12.1-gcc-4.8.5
source activate myenv
```

在提交job时，只需要加上一行命令就可以了：

```
source PATH/env.sh
```

其中`PATH`需要替换为为我们刚刚创建的`env.sh`文件所在的目录。

在Pi 2.0超算平台上，job是通过`sbatch`命令提交的，具体的来说我们需要先编写一个`.slurm`文件，然后在文件中添加对job的描述和job要执行的命令，例如：

```
#!/bin/bash
#SBATCH --job-name=hello_world
#SBATCH --partition=small
#SBATCH --ntasks-per-node=1
#SBATCH --mail-type=end
#SBATCH --mail-user=YOUR_EMAIL_ADDRESS
#SBATCH --output=%j.out
#SBATCH --error=%j.err
#SBATCH --time=00:10:00

export work_path=${HOME}/parallel_computing_example/python
source ${work_path}/env.sh
~/.conda/envs/myenv/bin/python ${work_path}/hellp_world.py
```

这里面，`--mail-user`需要替换成你们自己的邮箱，`work_path`需要替换成你们自己的工作目录。

第一条命令`export`相当于定义了一个字符串变量`work_path`，后续只需要通过`${work_path}`即可调用这个变量；

第二条命令`source`是加载`${work_path}env.sh`中定义的环境；

第三条命令是让`python`去运行`HelloWorld.py`脚本，这里我们具体指定了`python`程序所在的位置，避免和系统自带的`python`搞混了，同学们如果使用了不一样的环境名称，可能`python`所处的位置也会不一样（注意到路径中有个`/myenv`）；

参考资料：

[Pi超算平台用户文档](https://docs.hpc.sjtu.edu.cn/job/slurm/)

[[超算培训-1]2020-3-4线上培训](https://www.bilibili.com/video/BV1vE411x7HD)

## 提交CUDA作业

总体流程和提交python作业类似，也是需要加载环境，然后提交作业。不同点主要是：

- 加载cuda环境而不是conda环境
- cuda作业需要提交到GPU节点上
- cuda程序要先编译后执行

## Notes

### student账号作业提交出错

如果遇到下面的错误信息：

```
Batch job submission failed: Job violates accounting/QOS policy (job submit limit, user's size and/or time limits)
```

那说明你有一个作业正在作业队列中，可以通过`squeue`来查询作业队列。

有的时候作业队列中的作业会卡住而不能正常，可以通过`scancel {JOBID}`命令来取消作业。

### 如何传输文件

可以通过`scp`命令来传输，也可以通过studio.hpc网站上的文件管理系统来传输。
