num = 64

# step 1, initialize list
list_a = [i for i in range(num)]
list_b = [i for i in range(num)]
list_c = [0 for _ in range(num)]

# add list
for i in range(num):
    list_c[i] = list_a[i] + list_b[i]

# show result
print(list_c)


