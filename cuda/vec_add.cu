#include <stdio.h>
#include <stdlib.h>

#define N 8192
#define NUM_BLOCK 64

__global__ void device_add(int *device_a, 
                           int *device_b, 
                           int *device_c) 
{
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    device_c[index] = device_a[index] + device_b[index];
}

// Basically just fills the array with index.
void fill_array(int *data)
{
    for (int idx = 0; idx < N; idx++)
        data[idx] = idx;
}

void print_output(int *list_a, int *list_b, int *list_c)
{
    for (int idx = 0; idx < N; idx++)
        printf("\n %d + %d = %d", list_a[idx], list_b[idx], list_c[idx]);
}

int main(void) 
{
    int *list_a, *list_b, *list_c;
    int *device_a, *device_b, *device_c; // device copies of list_a, list_b, list_c
    size_t size = N * sizeof(int);

    // Alloc space for host copies of list_a, list_b, list_c and setup input values
    list_a = (int *)malloc(size); fill_array(list_a);
    list_b = (int *)malloc(size); fill_array(list_b);
    list_c = (int *)malloc(size);

    // Alloc space for device copies of vector (device_a, device_b, device_c)
    cudaMalloc(reinterpret_cast<void **>(&device_a), size);
    cudaMalloc(reinterpret_cast<void **>(&device_b), size);
    cudaMalloc(reinterpret_cast<void **>(&device_c), size);

    // Copy from host to device
    cudaMemcpy(device_a, list_a, size_t(size), cudaMemcpyHostToDevice);
    cudaMemcpy(device_b, list_b, size_t(N* sizeof(int)), cudaMemcpyHostToDevice);
    device_add<<<NUM_BLOCK, int(N / NUM_BLOCK)>>>(device_a,device_b,device_c);
    cudaDeviceSynchronize();

    // Copy result back to host
    cudaMemcpy(list_c, device_c, size, cudaMemcpyDeviceToHost);
    print_output(list_a, list_b, list_c);
    free(list_a); free(list_b); free(list_c);

    // Free gpu memory
    cudaFree(device_a); cudaFree(device_b); cudaFree(device_c);
    return 0;
}
